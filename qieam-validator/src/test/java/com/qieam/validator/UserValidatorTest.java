package com.qieam.validator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.UUID;

import javax.validation.Validation;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.qieam.dao.UserDAO;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.validator.impl.EntityValidationServiceImpl;
import com.qieam.validator.impl.UserValidatorImpl;

public class UserValidatorTest {

	private UserValidatorImpl userValidator;
	private String correctUserId;
	private String wrongUserId;
	
	@Before
	public void init() {
		this.userValidator = new UserValidatorImpl();
		this.correctUserId = UUID.randomUUID().toString();
		this.wrongUserId = "Test";
		ReflectionTestUtils.setField(this.userValidator, "userIdRegEx", "[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}");
		EntityValidationService entityValidationService = new EntityValidationServiceImpl();
		ReflectionTestUtils.setField(entityValidationService, "validator", Validation.buildDefaultValidatorFactory().getValidator());
		ReflectionTestUtils.setField(this.userValidator, "entityValidationService", entityValidationService);
	}
	
	@Test
	public void validateFindAllSuccess() throws Exception {
		assertTrue(this.userValidator.validateFindByUserId(this.correctUserId));
	}
	
	@Test
	public void validateFindAll400Error() {
		assertThrows(Http400Exception.class, () -> {
			this.userValidator.validateFindByUserId(this.wrongUserId);
	    });
	}
	
	@Test
	public void validatePostSuccess() throws Exception {
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("firstName", "Test");
		((ObjectNode)node).put("lastName", "Test");
		assertNotNull(this.userValidator.validatePost(node));
	}
	
	@Test
	public void validatePostNullFirstName() throws Exception {
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("firstName", "Test");
		assertThrows(Http400Exception.class, () -> {
			this.userValidator.validatePost(node);
	    });
	}
	
	@Test
	public void validatePostEmptyFirstName() throws Exception {
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("firstName", "");
		((ObjectNode)node).put("lastName", "Test");
		assertThrows(Http400Exception.class, () -> {
			this.userValidator.validatePost(node);
	    });
	}
	
	@Test
	public void validatePostNullLastName() throws Exception {
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("firstName", "Test");
		assertThrows(Http400Exception.class, () -> {
			this.userValidator.validatePost(node);
	    });
	}
	
	@Test
	public void validatePostEmptyLastName() throws Exception {
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("firstName", "Test");
		((ObjectNode)node).put("lastName", "");
		assertThrows(Http400Exception.class, () -> {
			this.userValidator.validatePost(node);
	    });
	}
	
	@Test
	public void validateDeleteSuccess() throws Exception {
		String userId = UUID.randomUUID().toString();
		User user = new User(userId, "",  "", new Date().getTime());
		
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("userId", userId);

		UserDAO userDAO = Mockito.mock(UserDAO.class);
	    Mockito.when(userDAO.findById(userId)).thenReturn(user);
	    ReflectionTestUtils.setField(this.userValidator, "userDAO", userDAO);
	    
		assertTrue(this.userValidator.validateDelete(userId));
	}
	
	@Test
	public void validateDelete204Error() {
		String userId = UUID.randomUUID().toString();
		
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("userId", userId);

		UserDAO userDAO = Mockito.mock(UserDAO.class);
	    Mockito.when(userDAO.findById(userId)).thenReturn(null);
	    ReflectionTestUtils.setField(this.userValidator, "userDAO", userDAO);
	    
		assertThrows(Http204Exception.class, () -> {
			this.userValidator.validateDelete(userId);
	    });
	}
}
