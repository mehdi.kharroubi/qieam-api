package com.qieam.validator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.UUID;

import javax.validation.Validation;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.qieam.dao.FriendDAO;
import com.qieam.dao.UserDAO;
import com.qieam.model.Friend;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http409Exception;
import com.qieam.validator.impl.EntityValidationServiceImpl;
import com.qieam.validator.impl.FriendValidatorImpl;

public class FriendValidatorTest {

	private FriendValidatorImpl friendValidator;
	private String correctUserId;
	private String wrongUserId;
	
	@Before
	public void init() {
		this.friendValidator = new FriendValidatorImpl();
		this.correctUserId = UUID.randomUUID().toString();
		this.wrongUserId = "Test";
		ReflectionTestUtils.setField(this.friendValidator, "userIdRegEx", "[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}");
		EntityValidationService entityValidationService = new EntityValidationServiceImpl();
		ReflectionTestUtils.setField(entityValidationService, "validator", Validation.buildDefaultValidatorFactory().getValidator());
		ReflectionTestUtils.setField(this.friendValidator, "entityValidationService", entityValidationService);
	}
	
	@Test
	public void validateFindAllSuccess() throws Exception {
		assertTrue(this.friendValidator.validateFindByUserId(this.correctUserId));
	}
	
	@Test
	public void validateFindAll400Error() {
		assertThrows(Http400Exception.class, () -> {
			this.friendValidator.validateFindByUserId(this.wrongUserId);
	    });
	}
	
	@Test
	public void validatePostSuccess() throws Exception {
		String firstUserId = UUID.randomUUID().toString();
		String secondUserId = UUID.randomUUID().toString();
		
		User firstUser = new User(firstUserId, "", 
				"", new Date().getTime());
		User secondUser = new User(firstUserId, "", 
				"", new Date().getTime());
		
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("friendId", secondUserId);

		FriendDAO friendDAO = Mockito.mock(FriendDAO.class);
	    Mockito.when(friendDAO.findById(firstUserId, secondUserId)).thenReturn(null);
	    ReflectionTestUtils.setField(this.friendValidator, "friendDAO", friendDAO);
		UserDAO userDAO = Mockito.mock(UserDAO.class);
	    Mockito.when(userDAO.findById(firstUserId)).thenReturn(firstUser);
	    Mockito.when(userDAO.findById(secondUserId)).thenReturn(secondUser);
	    ReflectionTestUtils.setField(this.friendValidator, "userDAO", userDAO);
		assertNotNull(this.friendValidator.validatePost(firstUserId, node));
	}
	
	@Test
	public void validatePost409Error() throws Exception {
		String firstUserId = UUID.randomUUID().toString();
		String secondUserId = UUID.randomUUID().toString();
		
		User firstUser = new User(firstUserId, "", 
				"", new Date().getTime());
		User secondUser = new User(firstUserId, "", 
				"", new Date().getTime());
		
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("friendId", secondUserId);

		Friend friend = new Friend(UUID.randomUUID().toString(), firstUserId,
				secondUserId, new Date().getTime());
		FriendDAO friendDAO = Mockito.mock(FriendDAO.class);
	    Mockito.when(friendDAO.findById(firstUserId, secondUserId)).thenReturn(friend);
	    ReflectionTestUtils.setField(this.friendValidator, "friendDAO", friendDAO);
		UserDAO userDAO = Mockito.mock(UserDAO.class);
	    Mockito.when(userDAO.findById(firstUserId)).thenReturn(firstUser);
	    Mockito.when(userDAO.findById(secondUserId)).thenReturn(secondUser);
	    ReflectionTestUtils.setField(this.friendValidator, "userDAO", userDAO);
		assertThrows(Http409Exception.class, () -> {
			this.friendValidator.validatePost(firstUserId, node);
	    });
	}
	
	@Test
	public void validateSecondUserNullPost204Error() throws Exception {
		String firstUserId = UUID.randomUUID().toString();
		String secondUserId = UUID.randomUUID().toString();
		
		User firstUser = new User(firstUserId, "", 
				"", new Date().getTime());
		
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("friendId", secondUserId);

		FriendDAO friendDAO = Mockito.mock(FriendDAO.class);
	    Mockito.when(friendDAO.findById(firstUserId, secondUserId)).thenReturn(null);
	    ReflectionTestUtils.setField(this.friendValidator, "friendDAO", friendDAO);
		UserDAO userDAO = Mockito.mock(UserDAO.class);
	    Mockito.when(userDAO.findById(firstUserId)).thenReturn(firstUser);
	    Mockito.when(userDAO.findById(secondUserId)).thenReturn(null);
	    ReflectionTestUtils.setField(this.friendValidator, "userDAO", userDAO);
		assertThrows(Http204Exception.class, () -> {
			this.friendValidator.validatePost(firstUserId, node);
	    });
	}
	
	@Test
	public void validatePostFirstUserNull204Error() throws Exception {
		String firstUserId = UUID.randomUUID().toString();
		String secondUserId = UUID.randomUUID().toString();
		
		User secondUser = new User(secondUserId, "", "", new Date().getTime());
		
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("friendId", secondUserId);
		
		FriendDAO friendDAO = Mockito.mock(FriendDAO.class);
	    Mockito.when(friendDAO.findById(firstUserId, secondUserId)).thenReturn(null);
	    ReflectionTestUtils.setField(this.friendValidator, "friendDAO", friendDAO);
		UserDAO userDAO = Mockito.mock(UserDAO.class);
	    Mockito.when(userDAO.findById(firstUserId)).thenReturn(null);
	    Mockito.when(userDAO.findById(secondUserId)).thenReturn(secondUser);
	    ReflectionTestUtils.setField(this.friendValidator, "userDAO", userDAO);
		assertThrows(Http204Exception.class, () -> {
			this.friendValidator.validatePost(firstUserId, node);
	    });
	}
	
	@Test
	public void validateDeleteSuccess() throws Exception {
		String firstUserId = UUID.randomUUID().toString();
		String secondUserId = UUID.randomUUID().toString();
		Friend friend = new Friend(UUID.randomUUID().toString(), firstUserId,
				secondUserId, new Date().getTime());
		FriendDAO friendDAO = Mockito.mock(FriendDAO.class);
	    Mockito.when(friendDAO.findById(firstUserId, secondUserId)).thenReturn(friend);
	    ReflectionTestUtils.setField(this.friendValidator, "friendDAO", friendDAO);
		assertNotNull(this.friendValidator.validateDelete(firstUserId, secondUserId));
	}
	
	@Test
	public void validateDelete204Error() {
		String firstUserId = UUID.randomUUID().toString();
		String secondUserId = UUID.randomUUID().toString();
		FriendDAO friendDAO = Mockito.mock(FriendDAO.class);
	    Mockito.when(friendDAO.findById(firstUserId, secondUserId)).thenReturn(null);
	    ReflectionTestUtils.setField(this.friendValidator, "friendDAO", friendDAO);
		assertThrows(Http204Exception.class, () -> {
			this.friendValidator.validateDelete(firstUserId, secondUserId);
	    });
	}
}
