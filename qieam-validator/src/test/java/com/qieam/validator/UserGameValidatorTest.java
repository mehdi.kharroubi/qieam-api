package com.qieam.validator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.UUID;

import javax.validation.Validation;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.qieam.dao.GameDAO;
import com.qieam.dao.UserDAO;
import com.qieam.dao.UserGameDAO;
import com.qieam.model.Game;
import com.qieam.model.User;
import com.qieam.model.UserGame;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http409Exception;
import com.qieam.validator.impl.EntityValidationServiceImpl;
import com.qieam.validator.impl.UserGameValidatorImpl;

public class UserGameValidatorTest {

	private UserGameValidatorImpl userGameValidator;
	private String correctUserId;
	private String wrongUserId;
	
	@Before
	public void init() {
		this.userGameValidator = new UserGameValidatorImpl();
		this.correctUserId = UUID.randomUUID().toString();
		this.wrongUserId = "Test";
		ReflectionTestUtils.setField(this.userGameValidator, "userIdRegEx", "[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}");
		EntityValidationService entityValidationService = new EntityValidationServiceImpl();
		ReflectionTestUtils.setField(entityValidationService, "validator", Validation.buildDefaultValidatorFactory().getValidator());
		ReflectionTestUtils.setField(this.userGameValidator, "entityValidationService", entityValidationService);
	}
	
	@Test
	public void validateFindAllSuccess() throws Exception {
		assertTrue(this.userGameValidator.validateFindByUserId(this.correctUserId));
	}
	
	@Test
	public void validateFindAll400Error() {
		assertThrows(Http400Exception.class, () -> {
			this.userGameValidator.validateFindByUserId(this.wrongUserId);
	    });
	}
	
	@Test
	public void validatePostSuccess() throws Exception {
		String firstUserId = UUID.randomUUID().toString();
		String gameId = UUID.randomUUID().toString();
		
		User firstUser = new User(firstUserId, "", 
				"", new Date().getTime());
		Game game = new Game(gameId, "",  "", false, new Date().getTime());
		
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("gameId", gameId);

		GameDAO gameDAO = Mockito.mock(GameDAO.class);
	    Mockito.when(gameDAO.findById(gameId)).thenReturn(game);
	    ReflectionTestUtils.setField(this.userGameValidator, "gameDAO", gameDAO);
	    
	    
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
	    Mockito.when(userGameDAO.findByUserIdAndGameId(firstUserId, gameId)).thenReturn(null);
		ReflectionTestUtils.setField(this.userGameValidator, "userGameDAO", userGameDAO);

		UserDAO userDAO = Mockito.mock(UserDAO.class);
	    Mockito.when(userDAO.findById(firstUserId)).thenReturn(firstUser);
	    ReflectionTestUtils.setField(this.userGameValidator, "userDAO", userDAO);
		assertNotNull(this.userGameValidator.validatePost(firstUserId, node));
		
	}
	
	@Test
	public void validatePost409Error() throws Exception {
		String firstUserId = UUID.randomUUID().toString();
		String gameId = UUID.randomUUID().toString();
		
		UserGame userGame = new UserGame(UUID.randomUUID().toString(), firstUserId, gameId, new Date().getTime());
		
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("gameId", gameId);
	    
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
	    Mockito.when(userGameDAO.findByUserIdAndGameId(firstUserId, gameId)).thenReturn(userGame);
		ReflectionTestUtils.setField(this.userGameValidator, "userGameDAO", userGameDAO);

		assertThrows(Http409Exception.class, () -> {
			this.userGameValidator.validatePost(firstUserId, node);
	    });
	}
	
	@Test
	public void validateUserNullPost204Error() throws Exception {
		String firstUserId = UUID.randomUUID().toString();
		String gameId = UUID.randomUUID().toString();
		
		Game game = new Game(gameId, "",  "", false, new Date().getTime());
		
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("gameId", gameId);

		GameDAO gameDAO = Mockito.mock(GameDAO.class);
	    Mockito.when(gameDAO.findById(gameId)).thenReturn(game);
	    ReflectionTestUtils.setField(this.userGameValidator, "gameDAO", gameDAO);
	    
	    
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
	    Mockito.when(userGameDAO.findByUserIdAndGameId(firstUserId, gameId)).thenReturn(null);
		ReflectionTestUtils.setField(this.userGameValidator, "userGameDAO", userGameDAO);

		UserDAO userDAO = Mockito.mock(UserDAO.class);
	    Mockito.when(userDAO.findById(firstUserId)).thenReturn(null);
	    ReflectionTestUtils.setField(this.userGameValidator, "userDAO", userDAO);
		assertThrows(Http204Exception.class, () -> {
			this.userGameValidator.validatePost(firstUserId, node);
	    });
	}
	
	@Test
	public void validatePostGameNull204Error() throws Exception {
		String firstUserId = UUID.randomUUID().toString();
		String gameId = UUID.randomUUID().toString();
		
		User firstUser = new User(firstUserId, "", 
				"", new Date().getTime());
		
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("gameId", gameId);

		GameDAO gameDAO = Mockito.mock(GameDAO.class);
	    Mockito.when(gameDAO.findById(gameId)).thenReturn(null);
	    ReflectionTestUtils.setField(this.userGameValidator, "gameDAO", gameDAO);
	    
	    
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
	    Mockito.when(userGameDAO.findByUserIdAndGameId(firstUserId, gameId)).thenReturn(null);
		ReflectionTestUtils.setField(this.userGameValidator, "userGameDAO", userGameDAO);

		UserDAO userDAO = Mockito.mock(UserDAO.class);
	    Mockito.when(userDAO.findById(firstUserId)).thenReturn(firstUser);
	    ReflectionTestUtils.setField(this.userGameValidator, "userDAO", userDAO);
		assertThrows(Http204Exception.class, () -> {
			this.userGameValidator.validatePost(firstUserId, node);
	    });
	}
}
