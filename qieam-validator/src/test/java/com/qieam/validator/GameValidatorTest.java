package com.qieam.validator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.UUID;

import javax.validation.Validation;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.qieam.dao.GameDAO;
import com.qieam.model.Game;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.validator.impl.EntityValidationServiceImpl;
import com.qieam.validator.impl.GameValidatorImpl;

public class GameValidatorTest {

	private GameValidatorImpl gameValidator;
	private String correctUserId;
	private String wrongUserId;
	
	@Before
	public void init() {
		this.gameValidator = new GameValidatorImpl();
		this.correctUserId = UUID.randomUUID().toString();
		this.wrongUserId = "Test";
		ReflectionTestUtils.setField(this.gameValidator, "userIdRegEx", "[0-9a-fA-F]{8}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{4}\\-[0-9a-fA-F]{12}");
		EntityValidationService entityValidationService = new EntityValidationServiceImpl();
		ReflectionTestUtils.setField(entityValidationService, "validator", Validation.buildDefaultValidatorFactory().getValidator());
		ReflectionTestUtils.setField(this.gameValidator, "entityValidationService", entityValidationService);
	}
	
	@Test
	public void validateFindAllSuccess() throws Exception {
		assertTrue(this.gameValidator.validateFindGameById(this.correctUserId));
	}
	
	@Test
	public void validateFindAll400Error() {
		assertThrows(Http400Exception.class, () -> {
			this.gameValidator.validateFindGameById(this.wrongUserId);
	    });
	}
	
	@Test
	public void validatePostSuccess() throws Exception {
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("title", "Test");
		((ObjectNode)node).put("coverUrl", "Test");
		assertNotNull(this.gameValidator.validatePost(node));
	}
	
	@Test
	public void validatePostNullTitle() throws Exception {
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("coverUrl", "Test");
		assertThrows(Http400Exception.class, () -> {
			this.gameValidator.validatePost(node);
	    });
	}
	
	@Test
	public void validatePostEmptyTitle() throws Exception {
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("title", "");
		((ObjectNode)node).put("coverUrl", "Test");
		assertThrows(Http400Exception.class, () -> {
			this.gameValidator.validatePost(node);
	    });
	}
	
	@Test
	public void validatePostNullCoverUrl() throws Exception {
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("title", "Test");
		assertThrows(Http400Exception.class, () -> {
			this.gameValidator.validatePost(node);
	    });
	}
	
	@Test
	public void validatePostEmptyCoverUrl() throws Exception {
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("title", "Test");
		((ObjectNode)node).put("coverUrl", "");
		assertThrows(Http400Exception.class, () -> {
			this.gameValidator.validatePost(node);
	    });
	}
	
	@Test
	public void validateDeleteSuccess() throws Exception {
		String gameId = UUID.randomUUID().toString();
		Game game = new Game(gameId, "",  "", false, new Date().getTime());
		
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("gameId", gameId);

		GameDAO gameDAO = Mockito.mock(GameDAO.class);
	    Mockito.when(gameDAO.findById(gameId)).thenReturn(game);
	    ReflectionTestUtils.setField(this.gameValidator, "gameDAO", gameDAO);
	    
		assertTrue(this.gameValidator.validateDelete(gameId));
	}
	
	@Test
	public void validateDelete204Error() {
		String gameId = UUID.randomUUID().toString();
		
		JsonNode node = JsonNodeFactory.instance.objectNode();
		((ObjectNode)node).put("gameId", gameId);

		GameDAO gameDAO = Mockito.mock(GameDAO.class);
	    Mockito.when(gameDAO.findById(gameId)).thenReturn(null);
	    ReflectionTestUtils.setField(this.gameValidator, "gameDAO", gameDAO);
	    
		assertThrows(Http204Exception.class, () -> {
			this.gameValidator.validateDelete(gameId);
	    });
	}
}
