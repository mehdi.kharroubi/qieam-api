package com.qieam.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.model.Game;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;

/**
 * Generic entity validation Service
 * 
 * @author Mehdi KHARROUBI
 *
 */
public interface GameValidator {

	/**
	 * Validate create a new game operation
	 * 
	 * @param newGameNode : new game data
	 * @return Created game
	 */
	Game validatePost(JsonNode jsonNode) throws Http400Exception;

	/**
	 * Validate delete a game by id operation
	 * 
	 * @param gameId : target game
	 * @return Operation result
	 * @throws Http400Exception 
	 */
	boolean validateDelete(String gameId) throws Http204Exception, Http400Exception;

	/**
	 * Validate find a game by id operation
	 * 
	 * @param gameId : id of the target game
	 * @return game data
	 */
	boolean validateFindGameById(String gameId) throws Http400Exception;

}
