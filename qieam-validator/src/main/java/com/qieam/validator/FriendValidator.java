package com.qieam.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.model.Friend;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http409Exception;

/**
 * Friend validation Service
 * 
 * @author Mehdi KHARROUBI
 *
 */
public interface FriendValidator {

	/**
	 * Validate add new friend for the given user operation
	 * 
	 * @param userId: user target
	 * @param newFriendNode : friend data
	 * @return Created friend
	 * @throws Exception
	 */
	Friend validatePost(String userId, JsonNode jsonNode) throws Http204Exception, Http400Exception, Http409Exception;

	/**
	 * Validate delete friend for the given user operation
	 * 
	 * @param userId : user target
	 * @param friendId : friend id
	 * @return Operation result
	 * @throws Http400Exception 
	 */
	String validateDelete(String firstUserId, String secondUserId) throws Http204Exception, Http400Exception;

	/**
	 * Validate find friends by user operation
	 * 
	 * @param userId : user target
	 * @return List of all friend for the given user
	 * @throws Exception
	 */
	boolean validateFindByUserId(String userId) throws Http400Exception;

}
