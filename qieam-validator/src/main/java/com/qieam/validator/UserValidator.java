package com.qieam.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;

/**
 * User validation Service
 * 
 * @author Mehdi KHARROUBI
 *
 */
public interface UserValidator {

	/**
	 * Validate create user operation
	 * 
	 * @param jsonNode : new user data
	 * @return created user
	 * @throws Exception
	 */
	User validatePost(JsonNode jsonNode) throws Http400Exception;

	/**
	 * Validate delete user operation
	 * 
	 * @param userId : target user
	 * @return operation result
	 * @throws Exception
	 */
	boolean validateDelete(String userId) throws Http400Exception, Http204Exception;

	/**
	 * Validate find user by id operation
	 * 
	 * @param userId: target user
	 * @return user data
	 * @throws Exception
	 */
	boolean validateFindByUserId(String userId) throws Http400Exception;

}
