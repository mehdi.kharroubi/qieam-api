package com.qieam.validator.impl;

import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.validation.ConstraintViolation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.dao.FriendDAO;
import com.qieam.dao.UserDAO;
import com.qieam.model.Friend;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http409Exception;
import com.qieam.validator.EntityValidationService;
import com.qieam.validator.FriendValidator;

/**
 * Friend validation Service implementation
 * 
 * @author Mehdi KHARROUBI
 *
 */
@Service
public class FriendValidatorImpl implements FriendValidator {

	/**
	 * User id format
	 */
	@Value("${uuid.regex}")
	private String userIdRegEx;

	/**
	 * Friend Id attribute name
	 */
	private static String FRIENDID = "friendId";

	/**
	 * Friend DAO
	 */
	@Autowired
	private FriendDAO friendDAO;
	
	/**
	 * User DAO
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * Javax model validation service
	 */
	@Autowired
	private EntityValidationService entityValidationService;

	/**
	 * @inheritDoc
	 */
	@Override
	public Friend validatePost(String userId, JsonNode jsonNode) throws Http204Exception, Http400Exception, Http409Exception {
		Friend friend = this.friendDAO.findById(userId, jsonNode.get(FRIENDID).asText());
		if (null != friend) {
			throw new Http409Exception();
		}
		User userOne = userDAO.findById(userId);
		User userTwo = userDAO.findById(jsonNode.get(FRIENDID).asText());
		if((null == userOne) || (null == userTwo)) {
			throw new Http204Exception();
		}
		friend = new Friend(UUID.randomUUID().toString(), userId,
				jsonNode.get(FRIENDID).asText(), new Date().getTime());
		Set<ConstraintViolation<Friend>> userViolation = entityValidationService.validate(friend);
		if ((null != userViolation) && !userViolation.isEmpty()) {
			throw new Http400Exception();
		}
		return friend;
	}

	/**
	 * @throws Http400Exception 
	 * @inheritDoc
	 */
	@Override
	public String validateDelete(String firstUserId, String secondUserId)
			throws Http204Exception, Http400Exception {
		if(!Pattern.matches(userIdRegEx, firstUserId) || !Pattern.matches(userIdRegEx, secondUserId)) {
			throw new Http400Exception();
		}
		Friend friend = this.friendDAO.findById(firstUserId, secondUserId);
		if (null == friend) {
			throw new Http204Exception();
		}
		return friend.getFriendId();
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean validateFindByUserId(String userId) throws Http400Exception {
		if(!Pattern.matches(userIdRegEx, userId)) {
			throw new Http400Exception();
		}
		return true;
	}
}
