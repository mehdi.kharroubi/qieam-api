package com.qieam.validator.impl;

import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.validation.ConstraintViolation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.dao.GameDAO;
import com.qieam.dao.UserDAO;
import com.qieam.dao.UserGameDAO;
import com.qieam.model.Game;
import com.qieam.model.User;
import com.qieam.model.UserGame;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http409Exception;
import com.qieam.validator.EntityValidationService;
import com.qieam.validator.UserGameValidator;

/**
 * User games validation Service implementation
 * 
 * @author Mehdi KHARROUBI
 *
 */
@Service
public class UserGameValidatorImpl implements UserGameValidator {

	/**
	 * Game Id attribute name
	 */
	private static String GAMEID = "gameId";
	
	/**
	 * User id format
	 */
	@Value("${uuid.regex}")
	private String userIdRegEx;
	
	/**
	 * Game DAO
	 */
	@Autowired
	private GameDAO gameDAO;
	
	/**
	 * User DAO
	 */
	@Autowired
	private UserDAO userDAO;
	
	/**
	 * User game DAO
	 */
	@Autowired
	private UserGameDAO userGameDAO;

	/**
	 * Javax model validation service
	 */
	@Autowired
	private EntityValidationService entityValidationService;

	/**
	 * @inheritDoc
	 */
	@Override
	public UserGame validatePost(String userId, JsonNode jsonNode) throws Http409Exception, Http204Exception, Http400Exception {
		UserGame userGame = this.userGameDAO.findByUserIdAndGameId(userId, jsonNode.get(GAMEID).asText());
		if (null != userGame) {
			throw new Http409Exception();
		}
		User user = userDAO.findById(userId);
		Game game = gameDAO.findById(jsonNode.get(GAMEID).asText());
		if((null == user) || (null == game)) {
			throw new Http204Exception();
		}
		userGame = new UserGame(UUID.randomUUID().toString(), userId,
				jsonNode.get(GAMEID).asText(), new Date().getTime());
		Set<ConstraintViolation<UserGame>> userViolation = entityValidationService.validate(userGame);
		if ((null != userViolation) && !userViolation.isEmpty()) {
			throw new Http400Exception();
		}
		return userGame;
	}

	/**
	 * @throws Http400Exception 
	 * @inheritDoc
	 */
	@Override
	public String validateDelete(String userId, String gameId)
			throws Http204Exception, Http400Exception {
		if(!Pattern.matches(userIdRegEx, userId) || !Pattern.matches(userIdRegEx, gameId)) {
			throw new Http400Exception();
		}
		UserGame userGame = this.userGameDAO.findByUserIdAndGameId(userId, gameId);
		if (null == userGame) {
			throw new Http204Exception();
		}
		return userGame.getUserGameId();
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean validateFindByUserId(String userId) throws Http400Exception {
		if(!Pattern.matches(userIdRegEx, userId)) {
			throw new Http400Exception();
		}
		return true;
	}
}
