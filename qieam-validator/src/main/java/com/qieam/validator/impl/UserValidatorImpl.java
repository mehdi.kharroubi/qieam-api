package com.qieam.validator.impl;

import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.validation.ConstraintViolation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.dao.UserDAO;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.validator.EntityValidationService;
import com.qieam.validator.UserValidator;

/**
 * User validation Service implementation
 * 
 * @author Mehdi KHARROUBI
 *
 */
@Service
public class UserValidatorImpl implements UserValidator {
	
	/**
	 * User id format
	 */
	@Value("${uuid.regex}")
	private String userIdRegEx;

	/**
	 * User DAO
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * Javax model validation service
	 */
	@Autowired
	private EntityValidationService entityValidationService;

	/**
	 * @inheritDoc
	 */
	@Override
	public User validatePost(JsonNode jsonNode) throws Http400Exception {
		if(!jsonNode.has("firstName") || !jsonNode.has("lastName")) {
			throw new Http400Exception();
		}
		User user = new User(UUID.randomUUID().toString(), jsonNode.get("firstName").asText(), 
				jsonNode.get("lastName").asText(), new Date().getTime());
		Set<ConstraintViolation<User>> userViolation = entityValidationService.validate(user);
		if ((null != userViolation) && !userViolation.isEmpty()) {
			throw new Http400Exception();
		}
		return user;
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean validateDelete(String userId) throws Http400Exception, Http204Exception {
		if(!Pattern.matches(userIdRegEx, userId)) {
			throw new Http400Exception();
		}
		User user = this.userDAO.findById(userId);
		if (null == user) {
			throw new Http204Exception();
		}
		return true;
	}
	
	/**
	 * @inheritDoc
	 */
	@Override
	public boolean validateFindByUserId(String userId) throws Http400Exception {
		if(!Pattern.matches(userIdRegEx, userId)) {
			throw new Http400Exception();
		}
		return true;
	}
}
