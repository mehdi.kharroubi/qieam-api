package com.qieam.validator.impl;

import java.util.Date;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.validation.ConstraintViolation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.dao.GameDAO;
import com.qieam.model.Game;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.validator.EntityValidationService;
import com.qieam.validator.GameValidator;

/**
 * Game validation Service implementation
 * 
 * @author Mehdi KHARROUBI
 *
 */
@Service
public class GameValidatorImpl implements GameValidator {

	/**
	 * User id format
	 */
	@Value("${uuid.regex}")
	private String userIdRegEx;
	
	/**
	 * Game DAO
	 */
	@Autowired
	private GameDAO gameDAO;

	/**
	 * Javax model validation service
	 */
	@Autowired
	private EntityValidationService entityValidationService;

	/**
	 * @inheritDoc
	 */
	@Override
	public Game validatePost(JsonNode jsonNode) throws Http400Exception {
		if(!jsonNode.has("title") || !jsonNode.has("coverUrl")) {
			throw new Http400Exception();
		}
		Game game = new Game(UUID.randomUUID().toString(), jsonNode.get("title").asText(), 
				jsonNode.get("coverUrl").asText(), false, new Date().getTime());
		Set<ConstraintViolation<Game>> userViolation = entityValidationService.validate(game);
		if ((null != userViolation) && !userViolation.isEmpty()) {
			throw new Http400Exception();
		}
		return game;
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean validateDelete(String gameId)
			throws Http204Exception, Http400Exception {
		if(!Pattern.matches(userIdRegEx, gameId)) {
			throw new Http400Exception();
		}
		Game game = this.gameDAO.findById(gameId);
		if (null == game) {
			throw new Http204Exception();
		}
		return true;
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean validateFindGameById(String gameId) throws Http400Exception {
		if(!Pattern.matches(userIdRegEx, gameId)) {
			throw new Http400Exception();
		}
		return true;
	}
}
