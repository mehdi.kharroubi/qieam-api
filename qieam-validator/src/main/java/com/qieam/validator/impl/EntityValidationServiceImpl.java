package com.qieam.validator.impl;

import java.util.Set;

import javax.annotation.PostConstruct;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.stereotype.Service;

import com.qieam.validator.EntityValidationService;

/**
 * Generic entity validation Service implementation Using Javax validation
 * 
 * @author Mehdi KHARROUBI
 *
 */
@Service
public class EntityValidationServiceImpl implements EntityValidationService {
	
	/**
	 * Object validator
	 */
	private Validator validator;
	
	/**
	 * init method
	 */
	@PostConstruct
	private void init() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public <T extends Object> Set<ConstraintViolation<T>> validate(T entity){
		return validator.validate(entity);
	}
}
