package com.qieam.validator;

import java.util.Set;

import javax.validation.ConstraintViolation;

/**
 * Generic entity validation Service Using Javax validation
 * 
 * @author Mehdi KHARROUBI
 *
 */
public interface EntityValidationService {

	/**
	 * Validate object using Javax validation Rules
	 * 
	 * @param <T>  : entity Type
	 * @param entity : entity to validate
	 * @return
	 */
	<T extends Object> Set<ConstraintViolation<T>> validate(T entity);

}