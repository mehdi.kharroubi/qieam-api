package com.qieam.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.model.UserGame;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http409Exception;

/**
 * User games validation Service
 * 
 * @author Mehdi KHARROUBI
 *
 */
public interface UserGameValidator {

	/**
	 * Validate find games by user operation
	 * 
	 * @param userId : user target
	 * @return List of all games for the given user
	 */
	boolean validateFindByUserId(String userId) throws Http400Exception;

	/**
	 * Validate delete game for the given user operation
	 *
	 * @param userId : user target
	 * @param gameId : game id
	 * @return Operation result
	 * @throws Http400Exception 
	 */
	String validateDelete(String userId, String gameId) throws Http204Exception, Http400Exception;

	/**
	 * Validate add new game for the given user operation
	 * 
	 * @param newUserGameNode : game data
	 * @param userId : target user
	 * @return Created user game
	 */
	UserGame validatePost(String userId, JsonNode jsonNode) throws Http409Exception, Http204Exception, Http400Exception;

}
