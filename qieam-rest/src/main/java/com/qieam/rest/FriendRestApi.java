package com.qieam.rest;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.api.FriendApiService;
import com.qieam.model.Friend;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http403Exception;
import com.qieam.model.exception.Http409Exception;

/**
 * Friends Rest Controller
 * 
 * @author Mehdi KHARROUBI
 *
 */
@RestController
@RequestMapping(value="/api/users/{userId}/friends")
public class FriendRestApi {
	
	/**
	 * Friend Api service
	 */
	@Autowired
	private FriendApiService friendApiService;
	
	/**
	 * Application logger
	 */
	Logger logger = LoggerFactory.getLogger(FriendRestApi.class);
	
	/**
	 * Find friends by user
	 * 
	 * @param response 
	 * @param userId : target user
	 * @return List of all friend for the given user
	 */
	@GetMapping(produces = "application/json")
	@ResponseBody
	public List<User> findAll(HttpServletResponse response, @PathVariable("userId") String userId) {
		try {
			return this.friendApiService.findByUserId(userId);
		} catch (Http400Exception ex) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} catch (Http204Exception ex) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} 
		return null;
	}
	
	/**
	 * Add new friend for the given user
	 * 
	 * @param response
	 * @param newFriendNode : friend data
	 * @param userId : target user
	 * @return Created friend
	 */
	@PostMapping(produces = "application/json")
	@ResponseBody
	public Friend post(HttpServletResponse response, @RequestBody JsonNode newFriendNode, @PathVariable("userId") String userId) {
		try {
			return this.friendApiService.post(userId, newFriendNode);
		} catch (Http400Exception ex) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} catch (Http204Exception ex) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		} catch (Http409Exception ex) {
			response.setStatus(HttpServletResponse.SC_CONFLICT);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} 
		return null;
	}

	/**
	 * Delete friend for the given user
	 * 
	 * @param response
	 * @param userId : target user
	 * @param friendId : friend id
	 * @return Operation result
	 */
	@DeleteMapping(value = "/{friendId}", produces = "application/json")
	@ResponseBody
	public boolean delete(HttpServletResponse response, @PathVariable("userId") String userId, @PathVariable("friendId") String friendId) {
		try {
			return this.friendApiService.delete(userId, friendId);
		} catch (Http400Exception ex) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} catch (Http204Exception ex) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} 
		return false;
	}
}