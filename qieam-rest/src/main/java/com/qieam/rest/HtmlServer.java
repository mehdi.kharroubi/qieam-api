package com.qieam.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HtmlServer {

    @GetMapping(value = {"/documentation"})
    public ModelAndView index() {
        return new ModelAndView("documentation/index");
    }
}
