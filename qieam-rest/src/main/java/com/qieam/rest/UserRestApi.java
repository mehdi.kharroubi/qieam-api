package com.qieam.rest;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.api.UserApiService;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http403Exception;
import com.qieam.model.exception.Http409Exception;

/**
 * User Rest Controller
 * 
 * @author Mehdi KHARROUBI
 *
 */
@RestController
@RequestMapping(value="/api/users")
public class UserRestApi {
	
	/**
	 * User Api service
	 */
	@Autowired
	private UserApiService userApiService;
	
	/**
	 * Application logger
	 */
	Logger logger = LoggerFactory.getLogger(UserRestApi.class);
	
	/**
	 * Find all users
	 * 
	 * @param response
	 * @return list of users
	 */
	@GetMapping(produces = "application/json")
	@ResponseBody
	public List<User> findAll(HttpServletResponse response) {
		try {
			return this.userApiService.findAll();
		} catch (Http204Exception ex) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} 
		return null;
	}
	
	/**
	 * Find a user by id
	 * 
	 * @param response
	 * @param userId : id of the target user
	 * @return user data
	 */
	@GetMapping(value = "/{userId}", produces = "application/json")
	@ResponseBody
	public User findByUserId(HttpServletResponse response, @PathVariable("userId") String userId) {
		try {
			return this.userApiService.findByUserId(userId);
		} catch (Http400Exception ex) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} catch (Http204Exception ex) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} 
		return null;
	}
	
	/**
	 * Create a new user
	 * 
	 * @param response
	 * @param newUserNode : new user data
	 * @return Created user
	 */
	@PostMapping(produces = "application/json")
	@ResponseBody
	public User post(HttpServletResponse response, @RequestBody JsonNode newUserNode) {
		try {
			return this.userApiService.post(newUserNode);
		} catch (Http400Exception ex) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} 
		return null;
	}

	/**
	 * Delete a user by id
	 * 
	 * @param response
	 * @param userId : target user
	 * @return Operation result
	 */
	@DeleteMapping(value = "/{userId}", produces = "application/json")
	@ResponseBody
	public boolean delete(HttpServletResponse response, @PathVariable("userId") String userId) {
		try {
			return this.userApiService.delete(userId);
		} catch (Http400Exception ex) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} catch (Http204Exception ex) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} 
		return false;
	}
}