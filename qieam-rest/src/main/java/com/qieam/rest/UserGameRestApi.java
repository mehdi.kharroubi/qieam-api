package com.qieam.rest;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.api.UserGameApiService;
import com.qieam.model.Game;
import com.qieam.model.UserGame;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http403Exception;
import com.qieam.model.exception.Http409Exception;

/**
 * User games Rest Controller
 * 
 * @author Mehdi KHARROUBI
 *
 */
@RestController
@RequestMapping(value="/api/users/{userId}/games")
public class UserGameRestApi {
	
	/**
	 * User games Api service
	 */
	@Autowired
	private UserGameApiService userGameApiService;
	
	/**
	 * Application logger
	 */
	Logger logger = LoggerFactory.getLogger(UserGameRestApi.class);
	
	/**
	 * Find games by user
	 * 
	 * @param response 
	 * @param userId : user target
	 * @return List of all games for the given user
	 */
	@GetMapping(produces = "application/json")
	@ResponseBody
	public List<Game> findByUserId(HttpServletResponse response, @PathVariable("userId") String userId) {
		try {
			return this.userGameApiService.findByUserId(userId);
		} catch (Http400Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} catch (Http204Exception ex) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		}
		return null;
	}
	
	/**
	 * Add new game for the given user
	 * 
	 * @param response
	 * @param newUserGameNode : game data
	 * @param userId : target user
	 * @return Created user game
	 */
	@PostMapping(produces = "application/json")
	@ResponseBody
	public UserGame post(HttpServletResponse response, @RequestBody JsonNode newUserGameNode, @PathVariable("userId") String userId) {
		try {
			return this.userGameApiService.post(userId, newUserGameNode);
		} catch (Http400Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} catch (Http204Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		} catch (Http409Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_CONFLICT);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} 
		return null;
	}

	/**
	 * Delete friend for the given user
	 * 
	 * @param response
	 * @param userId : user target
	 * @param friendId : friend id
	 * @return Operation result
	 */
	@DeleteMapping(value = "/{gameId}", produces = "application/json")
	@ResponseBody
	public boolean delete(HttpServletResponse response, @PathVariable("userId") String userId, @PathVariable("gameId") String gameId) {
		try {
			return this.userGameApiService.delete(userId, gameId);
		} catch (Http400Exception ex) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} catch (Http204Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} 
		return false;
	}
}