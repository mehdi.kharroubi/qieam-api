package com.qieam.rest;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.api.GameApiService;
import com.qieam.model.Game;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http403Exception;
import com.qieam.model.exception.Http409Exception;

/**
 * Game Rest Controller
 * 
 * @author Mehdi KHARROUBI
 *
 */
@RestController
@RequestMapping(value="/api/store/games")
public class GameRestApi {
	
	/**
	 * Game Api service
	 */
	@Autowired
	private GameApiService gameApiService;
	
	/**
	 * Application logger
	 */
	Logger logger = LoggerFactory.getLogger(GameRestApi.class);
	
	/**
	 * Find all games in store
	 * 
	 * @param response
	 * @return list of games
	 */
	@GetMapping(produces = "application/json")
	@ResponseBody
	public List<Game> findAll(HttpServletResponse response) {
		try {
			return this.gameApiService.findAll();
		} catch (Http204Exception ex) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} 
		return null;
	}
	
	/**
	 * Find a game by id
	 * 
	 * @param response
	 * @param gameId : id of the target game
	 * @return game data
	 */
	@GetMapping(value = "/{gameId}", produces = "application/json")
	@ResponseBody
	public Game findByGameId(HttpServletResponse response, @PathVariable("gameId") String gameId) {
		try {
			return this.gameApiService.findByGameId(gameId);
		} catch (Http400Exception ex) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} catch (Http204Exception ex) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} 
		return null;
	}
	
	/**
	 * Create a new game
	 * 
	 * @param response
	 * @param newGameNode : new game data
	 * @return Created game
	 */
	@PostMapping(produces = "application/json")
	@ResponseBody
	public Game post(HttpServletResponse response, @RequestBody JsonNode newGameNode) {
		try {
			return this.gameApiService.post(newGameNode);
		} catch (Http400Exception ex) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} 
		return null;
	}

	/**
	 * Delete a game by id
	 * 
	 * @param response
	 * @param gameId : target game
	 * @return Operation result
	 */
	@DeleteMapping(value = "/{gameId}", produces = "application/json")
	@ResponseBody
	public boolean delete(HttpServletResponse response, @PathVariable("gameId") String gameId) {
		try {
			return this.gameApiService.delete(gameId);
		} catch (Http400Exception ex) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		} catch (Http204Exception ex) {
			response.setStatus(HttpServletResponse.SC_NO_CONTENT);
		} catch (Exception ex) {
			logger.error(ex.getMessage(), ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} 
		return false;
	}
}