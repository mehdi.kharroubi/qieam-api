package com.qieam.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import com.qieam.dao.UserDAO;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http500Exception;
import com.qieam.service.impl.UserServiceImpl;

public class UserServiceTest {

	private UserServiceImpl userService;
	private String correctUserId;
	
	@Before
	public void init() {
		this.userService = new UserServiceImpl();
		this.correctUserId = UUID.randomUUID().toString();
	}
	
	@Test
	public void validateFindOneSuccess() throws Exception {
		User user = new User(this.correctUserId, "",  "", new Date().getTime());
		UserDAO userDAO = Mockito.mock(UserDAO.class);
		ReflectionTestUtils.setField(this.userService, "userDAO", userDAO);
		Mockito.when(userDAO.findById(this.correctUserId)).thenReturn(user);
		User resultUser = this.userService.findByUserId(this.correctUserId);
		assertNotNull(resultUser);
		assertEquals(resultUser, user);
	}
	
	@Test
	public void validateFindOne204Error() {
		UserDAO userDAO = Mockito.mock(UserDAO.class);
		ReflectionTestUtils.setField(this.userService, "userDAO", userDAO);
		Mockito.when(userDAO.findById(this.correctUserId)).thenReturn(null);
		assertThrows(Http204Exception.class, () -> {
			this.userService.findByUserId(this.correctUserId);
	    });
	}
	
	@Test
	public void validateFindAllSuccess() throws Exception {
		User userA = new User(UUID.randomUUID().toString(), "",  "", new Date().getTime());
		User userB = new User(UUID.randomUUID().toString(), "",  "", new Date().getTime());
		List<User> userList = new ArrayList<User>();
		userList.add(userA);
		userList.add(userB);
		UserDAO userDAO = Mockito.mock(UserDAO.class);
		ReflectionTestUtils.setField(this.userService, "userDAO", userDAO);
		Mockito.when(userDAO.findAll()).thenReturn(userList);
		List<User> result = this.userService.findAll();
		assertNotNull(result);
		assertEquals(result.size(), userList.size());
	}
	
	@Test
	public void validateFindAll204Error() {
		UserDAO userDAO = Mockito.mock(UserDAO.class);
		ReflectionTestUtils.setField(this.userService, "userDAO", userDAO);
		 Mockito.when(userDAO.findAll()).thenReturn(new ArrayList<User>());
		assertThrows(Http204Exception.class, () -> {
			this.userService.findAll();
	    });
	}
	
	@Test
	public void validatePostSuccess() throws Exception {
		User user = new User(UUID.randomUUID().toString(), "",  "", new Date().getTime());
		UserDAO userDAO = Mockito.mock(UserDAO.class);
		ReflectionTestUtils.setField(this.userService, "userDAO", userDAO);
		Mockito.when(userDAO.insert(user)).thenReturn(1L);
		assertNotNull(this.userService.post(user));
	}
	
	@Test
	public void validatePost500Error() throws Exception {
		User user = new User(UUID.randomUUID().toString(), "",  "", new Date().getTime());
		UserDAO userDAO = Mockito.mock(UserDAO.class);
		ReflectionTestUtils.setField(this.userService, "userDAO", userDAO);
		Mockito.when(userDAO.insert(user)).thenReturn(0L);
		assertThrows(Http500Exception.class, () -> {
			this.userService.post(user);
	    });
	}
	
	@Test
	public void validateDeleteSuccess() throws Exception {
		UserDAO userDAO = Mockito.mock(UserDAO.class);
	    Mockito.when(userDAO.delete(this.correctUserId)).thenReturn(1L);
	    ReflectionTestUtils.setField(this.userService, "userDAO", userDAO);
		assertTrue(this.userService.delete(this.correctUserId));
	}
	
	@Test
	public void validateDelete500Error() {
		UserDAO userDAO = Mockito.mock(UserDAO.class);
	    Mockito.when(userDAO.delete(this.correctUserId)).thenReturn(0L);
	    ReflectionTestUtils.setField(this.userService, "userDAO", userDAO);
		assertThrows(Http500Exception.class, () -> {
			this.userService.delete(this.correctUserId);
	    });
	}
}
