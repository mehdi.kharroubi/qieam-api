package com.qieam.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import com.qieam.dao.FriendDAO;
import com.qieam.model.Friend;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http500Exception;
import com.qieam.service.impl.FriendServiceImpl;

public class FriendServiceTest {

	private FriendServiceImpl friendService;
	private String correctFriendId;
	
	@Before
	public void init() {
		this.friendService = new FriendServiceImpl();
		this.correctFriendId = UUID.randomUUID().toString();
	}
	
	@Test
	public void validateFindAllSuccess() throws Exception {
		String userId = UUID.randomUUID().toString();
		User friendA = new User(UUID.randomUUID().toString(), "",  "", new Date().getTime());
		User friendB = new User(UUID.randomUUID().toString(), "",  "", new Date().getTime());
		List<User> friendList = new ArrayList<User>();
		friendList.add(friendA);
		friendList.add(friendB);
		FriendDAO friendDAO = Mockito.mock(FriendDAO.class);
		ReflectionTestUtils.setField(this.friendService, "friendDAO", friendDAO);
		Mockito.when(friendDAO.findByUserId(userId)).thenReturn(friendList);
		List<User> result = this.friendService.findByUserId(userId);
		assertNotNull(result);
		assertEquals(result.size(), friendList.size());
	}
	
	@Test
	public void validateFindAll204Error() {
		String userId = UUID.randomUUID().toString();
		FriendDAO friendDAO = Mockito.mock(FriendDAO.class);
		ReflectionTestUtils.setField(this.friendService, "friendDAO", friendDAO);
		Mockito.when(friendDAO.findByUserId(userId)).thenReturn(new ArrayList<User>());
		assertThrows(Http204Exception.class, () -> {
			this.friendService.findByUserId(userId);
	    });
	}
	
	@Test
	public void validatePostSuccess() throws Exception {
		Friend friend = new Friend(UUID.randomUUID().toString(), "",  "", new Date().getTime());
		FriendDAO friendDAO = Mockito.mock(FriendDAO.class);
		ReflectionTestUtils.setField(this.friendService, "friendDAO", friendDAO);
		Mockito.when(friendDAO.insert(friend)).thenReturn(1L);
		assertNotNull(this.friendService.post(friend));
	}
	
	@Test
	public void validatePost500Error() throws Exception {
		Friend friend = new Friend(UUID.randomUUID().toString(), "",  "", new Date().getTime());
		FriendDAO friendDAO = Mockito.mock(FriendDAO.class);
		ReflectionTestUtils.setField(this.friendService, "friendDAO", friendDAO);
		Mockito.when(friendDAO.insert(friend)).thenReturn(0L);
		assertThrows(Http500Exception.class, () -> {
			this.friendService.post(friend);
	    });
	}
	
	@Test
	public void validateDeleteSuccess() throws Exception {
		FriendDAO friendDAO = Mockito.mock(FriendDAO.class);
	    Mockito.when(friendDAO.delete(this.correctFriendId)).thenReturn(1L);
	    ReflectionTestUtils.setField(this.friendService, "friendDAO", friendDAO);
		assertTrue(this.friendService.delete(this.correctFriendId));
	}
	
	@Test
	public void validateDelete500Error() {
		FriendDAO friendDAO = Mockito.mock(FriendDAO.class);
	    Mockito.when(friendDAO.delete(this.correctFriendId)).thenReturn(0L);
	    ReflectionTestUtils.setField(this.friendService, "friendDAO", friendDAO);
		assertThrows(Http500Exception.class, () -> {
			this.friendService.delete(this.correctFriendId);
	    });
	}
}
