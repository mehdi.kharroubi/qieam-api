package com.qieam.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import com.qieam.dao.GameDAO;
import com.qieam.dao.UserGameDAO;
import com.qieam.model.UserGame;
import com.qieam.model.Game;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http500Exception;
import com.qieam.service.impl.UserGameServiceImpl;

public class UserGameServiceTest {

	private UserGameServiceImpl userGameService;
	private String correctUserGameId;
	
	@Before
	public void init() {
		this.userGameService = new UserGameServiceImpl();
		this.correctUserGameId = UUID.randomUUID().toString();
	}
	
	@Test
	public void validateFindAllSuccess() throws Exception {
		String userId = UUID.randomUUID().toString();
		Game userGameA = new Game(UUID.randomUUID().toString(), "",  "", false, new Date().getTime());
		Game userGameB = new Game(UUID.randomUUID().toString(), "",  "", false, new Date().getTime());
		List<Game> userGameList = new ArrayList<>();
		userGameList.add(userGameA);
		userGameList.add(userGameB);
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
		ReflectionTestUtils.setField(this.userGameService, "userGameDAO", userGameDAO);
		Mockito.when(userGameDAO.findByUserId(userId)).thenReturn(userGameList);
		List<Game> result = this.userGameService.findByUserId(userId);
		assertNotNull(result);
		assertEquals(result.size(), userGameList.size());
	}
	
	@Test
	public void validateFindAll204Error() {
		String userId = UUID.randomUUID().toString();
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
		ReflectionTestUtils.setField(this.userGameService, "userGameDAO", userGameDAO);
		Mockito.when(userGameDAO.findByUserId(userId)).thenReturn(new ArrayList<Game>());
		assertThrows(Http204Exception.class, () -> {
			this.userGameService.findByUserId(userId);
	    });
	}
	
	@Test
	public void validatePostSuccess() throws Exception {
		UserGame userGame = new UserGame(UUID.randomUUID().toString(), "",  "", new Date().getTime());
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
		ReflectionTestUtils.setField(this.userGameService, "userGameDAO", userGameDAO);
		Mockito.when(userGameDAO.insert(userGame)).thenReturn(1L);
		assertNotNull(this.userGameService.post(userGame));
	}
	
	@Test
	public void validatePost500Error() throws Exception {
		UserGame userGame = new UserGame(UUID.randomUUID().toString(), "",  "", new Date().getTime());
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
		ReflectionTestUtils.setField(this.userGameService, "userGameDAO", userGameDAO);
		Mockito.when(userGameDAO.insert(userGame)).thenReturn(0L);
		assertThrows(Http500Exception.class, () -> {
			this.userGameService.post(userGame);
	    });
	}
	
	@Test
	public void validateDeleteSuccess() throws Exception {
		UserGame userGameA = new UserGame(UUID.randomUUID().toString(), "",  "", new Date().getTime());
		UserGame userGameB = new UserGame(UUID.randomUUID().toString(), "",  "", new Date().getTime());
		List<UserGame> userGameList = new ArrayList<>();
		userGameList.add(userGameA);
		userGameList.add(userGameB);
		String gameId = UUID.randomUUID().toString();
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
		Mockito.when(userGameDAO.findByGameId(gameId)).thenReturn(userGameList);
	    Mockito.when(userGameDAO.delete(this.correctUserGameId)).thenReturn(1L);
	    ReflectionTestUtils.setField(this.userGameService, "userGameDAO", userGameDAO);
		assertTrue(this.userGameService.delete(gameId, this.correctUserGameId));
	}
	
	@Test
	public void validateDeleteSuccessWithGameDelete() throws Exception {
		String gameId = UUID.randomUUID().toString();
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
		Mockito.when(userGameDAO.findByGameId(gameId)).thenReturn(new ArrayList<>());
	    Mockito.when(userGameDAO.delete(this.correctUserGameId)).thenReturn(1L);
		GameDAO gameDAO = Mockito.mock(GameDAO.class);
		ReflectionTestUtils.setField(this.userGameService, "gameDAO", gameDAO);
		Mockito.when(gameDAO.delete(gameId)).thenReturn(1L);
	    ReflectionTestUtils.setField(this.userGameService, "userGameDAO", userGameDAO);
		assertTrue(this.userGameService.delete(gameId, this.correctUserGameId));
	}
	
	@Test
	public void validateDelete500Error() {
		String gameId = UUID.randomUUID().toString();
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
	    Mockito.when(userGameDAO.delete(this.correctUserGameId)).thenReturn(0L);
	    ReflectionTestUtils.setField(this.userGameService, "userGameDAO", userGameDAO);
		assertThrows(Http500Exception.class, () -> {
			this.userGameService.delete(gameId, this.correctUserGameId);
	    });
	}
}
