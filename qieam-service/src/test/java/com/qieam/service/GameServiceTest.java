package com.qieam.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import com.qieam.dao.GameDAO;
import com.qieam.dao.UserGameDAO;
import com.qieam.model.Game;
import com.qieam.model.UserGame;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http500Exception;
import com.qieam.service.impl.GameServiceImpl;

public class GameServiceTest {

	private GameServiceImpl gameService;
	private String correctGameId;
	
	@Before
	public void init() {
		this.gameService = new GameServiceImpl();
		this.correctGameId = UUID.randomUUID().toString();
	}
	
	@Test
	public void validateFindOneSuccess() throws Exception {
		Game game = new Game(this.correctGameId, "",  "", false, new Date().getTime());
		GameDAO gameDAO = Mockito.mock(GameDAO.class);
		ReflectionTestUtils.setField(this.gameService, "gameDAO", gameDAO);
		Mockito.when(gameDAO.findById(this.correctGameId)).thenReturn(game);
		Game resultGame = this.gameService.findGameById(this.correctGameId);
		assertNotNull(resultGame);
		assertEquals(resultGame, game);
	}
	
	@Test
	public void validateFindOne204Error() {
		GameDAO gameDAO = Mockito.mock(GameDAO.class);
		ReflectionTestUtils.setField(this.gameService, "gameDAO", gameDAO);
		Mockito.when(gameDAO.findById(this.correctGameId)).thenReturn(null);
		assertThrows(Http204Exception.class, () -> {
			this.gameService.findGameById(this.correctGameId);
	    });
	}
	
	@Test
	public void validateFindAllSuccess() throws Exception {
		Game gameA = new Game(UUID.randomUUID().toString(), "",  "", false, new Date().getTime());
		Game gameB = new Game(UUID.randomUUID().toString(), "",  "", false, new Date().getTime());
		List<Game> gameList = new ArrayList<Game>();
		gameList.add(gameA);
		gameList.add(gameB);
		GameDAO gameDAO = Mockito.mock(GameDAO.class);
		ReflectionTestUtils.setField(this.gameService, "gameDAO", gameDAO);
		Mockito.when(gameDAO.findAll()).thenReturn(gameList);
		List<Game> result = this.gameService.findAll();
		assertNotNull(result);
		assertEquals(result.size(), gameList.size());
	}
	
	@Test
	public void validateFindAll204Error() {
		GameDAO gameDAO = Mockito.mock(GameDAO.class);
		ReflectionTestUtils.setField(this.gameService, "gameDAO", gameDAO);
		 Mockito.when(gameDAO.findAll()).thenReturn(new ArrayList<Game>());
		assertThrows(Http204Exception.class, () -> {
			this.gameService.findAll();
	    });
	}
	
	@Test
	public void validatePostSuccess() throws Exception {
		Game game = new Game(UUID.randomUUID().toString(), "",  "", false, new Date().getTime());
		GameDAO gameDAO = Mockito.mock(GameDAO.class);
		ReflectionTestUtils.setField(this.gameService, "gameDAO", gameDAO);
		Mockito.when(gameDAO.insert(game)).thenReturn(1L);
		assertNotNull(this.gameService.post(game));
	}
	
	@Test
	public void validatePost500Error() throws Exception {
		Game game = new Game(UUID.randomUUID().toString(), "",  "", false, new Date().getTime());
		GameDAO gameDAO = Mockito.mock(GameDAO.class);
		ReflectionTestUtils.setField(this.gameService, "gameDAO", gameDAO);
		Mockito.when(gameDAO.insert(game)).thenReturn(0L);
		assertThrows(Http500Exception.class, () -> {
			this.gameService.post(game);
	    });
	}
	
	
	
	@Test
	public void validateDeleteRelatedUserGameSuccess() throws Exception {
		GameDAO gameDAO = Mockito.mock(GameDAO.class);
	    Mockito.when(gameDAO.disable(this.correctGameId)).thenReturn(1L);
	    ReflectionTestUtils.setField(this.gameService, "gameDAO", gameDAO);
	    
	    UserGame userGameA = new UserGame(UUID.randomUUID().toString(), "", this.correctGameId, new Date().getTime());
	    UserGame userGameB = new UserGame(UUID.randomUUID().toString(), "", this.correctGameId, new Date().getTime());
	    
	    List<UserGame> userGameList = new ArrayList<>();
	    userGameList.add(userGameA);
	    userGameList.add(userGameB);
	    
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
		Mockito.when(userGameDAO.findByGameId(this.correctGameId)).thenReturn(userGameList);
		ReflectionTestUtils.setField(this.gameService, "userGameDAO", userGameDAO);
	    
		assertTrue(this.gameService.delete(this.correctGameId));
	}
	
	@Test
	public void validateDeleteNoRelatedUserGameSuccess() throws Exception {
		GameDAO gameDAO = Mockito.mock(GameDAO.class);
	    Mockito.when(gameDAO.delete(this.correctGameId)).thenReturn(1L);
	    ReflectionTestUtils.setField(this.gameService, "gameDAO", gameDAO);
	    
	    List<UserGame> userGameList = new ArrayList<>();
	    
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
		Mockito.when(userGameDAO.findByGameId(this.correctGameId)).thenReturn(userGameList);
		ReflectionTestUtils.setField(this.gameService, "userGameDAO", userGameDAO);
	    
		assertTrue(this.gameService.delete(this.correctGameId));
	}
	
	@Test
	public void validateDeleteNoRelated500Error() {
		GameDAO gameDAO = Mockito.mock(GameDAO.class);
	    Mockito.when(gameDAO.disable(this.correctGameId)).thenReturn(0L);
	    ReflectionTestUtils.setField(this.gameService, "gameDAO", gameDAO);
	    
	    List<UserGame> userGameList = new ArrayList<>();
	    
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
		Mockito.when(userGameDAO.findByGameId(this.correctGameId)).thenReturn(userGameList);
		ReflectionTestUtils.setField(this.gameService, "userGameDAO", userGameDAO);
	    
		assertThrows(Http500Exception.class, () -> {
			this.gameService.delete(this.correctGameId);
	    });
	}
	
	@Test
	public void validateDeleteRelated500Error() {
		GameDAO gameDAO = Mockito.mock(GameDAO.class);
	    Mockito.when(gameDAO.delete(this.correctGameId)).thenReturn(0L);
	    ReflectionTestUtils.setField(this.gameService, "gameDAO", gameDAO);
	    
	    List<UserGame> userGameList = new ArrayList<>();
	    
		UserGameDAO userGameDAO = Mockito.mock(UserGameDAO.class);
		Mockito.when(userGameDAO.findByGameId(this.correctGameId)).thenReturn(userGameList);
		ReflectionTestUtils.setField(this.gameService, "userGameDAO", userGameDAO);
	    
		assertThrows(Http500Exception.class, () -> {
			this.gameService.delete(this.correctGameId);
	    });
	}
}
