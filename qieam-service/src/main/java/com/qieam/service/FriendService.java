package com.qieam.service;

import java.util.List;

import com.qieam.model.Friend;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http500Exception;

/**
 * Friend modification Service
 * 
 * @author Mehdi KHARROUBI
 *
 */
public interface FriendService {

	/**
	 * Find friends by user
	 * 
	 * @param userId : user target
	 * @return List of all friend for the given user
	 * @throws Exception
	 */
	List<User> findByUserId(String userId) throws Http204Exception;

	/**
	 * Add new friend for the given user
	 * 
	 * @param userId: user target
	 * @param friend : friend data
	 * @return Created friend
	 * @throws Exception
	 */
	Friend post(Friend friend) throws Http500Exception;

	/**
	 * Delete friend for the given user
	 * 
	 * @param userId : user target
	 * @param friendId : friend id
	 * @return Operation result
	 */
	boolean delete(String friendId) throws Http500Exception;

}