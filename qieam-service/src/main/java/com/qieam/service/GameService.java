package com.qieam.service;

import java.util.List;

import com.qieam.model.Game;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http500Exception;

/**
 * Game modification Service
 * 
 * @author Mehdi KHARROUBI
 *
 */
public interface GameService {

	/**
	 * Create a new game
	 * 
	 * @param game : new game data
	 * @return Created game
	 */
	Game post(Game game) throws Http500Exception;

	/**
	 * Find a game by id
	 * 
	 * @param gameId : id of the target game
	 * @return game data
	 */
	Game findGameById(String gameId) throws Http204Exception;

	/**
	 * Find all games
	 * 
	 * @return games list
	 */
	List<Game> findAll() throws Http204Exception;

	/**
	 * Delete a game by id
	 * 
	 * @param gameId : target game
	 * @return Operation result
	 */
	boolean delete(String gameId) throws Http500Exception;

}