package com.qieam.service;

import java.util.List;

import com.qieam.model.Game;
import com.qieam.model.UserGame;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http500Exception;

/**
 * User games modification Service
 * 
 * @author Mehdi KHARROUBI
 *
 */
public interface UserGameService {

	/**
	 * Add new game for the given user
	 * 
	 * @param userGame : game data
	 * @param userId : target user
	 * @return Created user game
	 */
	UserGame post(UserGame userGame) throws Http500Exception;

	/**
	 * Delete game for the given user
	 *
	 * @param userId : user target
	 * @param userGameId : user game id
	 * @return Operation result
	 */
	boolean delete(String gameId, String userGameId) throws Http500Exception;

	/**
	 * Find games by user
	 * 
	 * @param userId : user target
	 * @return List of all games for the given user
	 */
	List<Game> findByUserId(String userId) throws Http204Exception;

}