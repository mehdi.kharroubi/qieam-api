package com.qieam.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qieam.dao.FriendDAO;
import com.qieam.model.Friend;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http500Exception;
import com.qieam.service.FriendService;

/**
 * Friend modification Service implementation
 * 
 * @author Mehdi KHARROUBI
 *
 */
@Service
public class FriendServiceImpl implements FriendService {
	
	/**
	 * Friend DAO
	 */
	@Autowired
	private FriendDAO friendDAO;
	
	/**
	 * @inheritDoc
	 */
	@Override
	public List<User> findByUserId(String userId) throws Http204Exception {
		List<User> userList =  this.friendDAO.findByUserId(userId);
		if((null == userList) || userList.isEmpty()) {
			throw new Http204Exception();
		}
		return userList;
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public Friend post(Friend friend)
			throws Http500Exception {
		if (1 == this.friendDAO.insert(friend)) {
			return friend;
		}
		throw new Http500Exception();
	}
	
	/**
	 * @inheritDoc
	 */
	@Override
	public boolean delete(String friendId) throws Http500Exception {
		if (1 == this.friendDAO.delete(friendId)) {
			return true;
		}
		throw new Http500Exception();
	}
}
