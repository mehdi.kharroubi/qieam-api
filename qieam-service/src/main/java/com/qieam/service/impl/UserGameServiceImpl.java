package com.qieam.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qieam.dao.GameDAO;
import com.qieam.dao.UserGameDAO;
import com.qieam.model.Game;
import com.qieam.model.UserGame;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http500Exception;
import com.qieam.service.UserGameService;

/**
 * User game modification Service implementation
 * 
 * @author Mehdi KHARROUBI
 *
 */
@Service
public class UserGameServiceImpl implements UserGameService {

	/**
	 * User games DAO
	 */
	@Autowired
	private UserGameDAO userGameDAO;

	/**
	 * Game DAO
	 */
	@Autowired
	private GameDAO gameDAO;

	/**
	 * @inheritDoc
	 */
	@Override
	public UserGame post(UserGame userGame)
			throws Http500Exception {
		if (1 == this.userGameDAO.insert(userGame)) {
			return userGame;
		}
		throw new Http500Exception();
	}
	
	/**
	 * @inheritDoc
	 */
	@Override
	public boolean delete(String gameId, String userGameId) throws Http500Exception {
		if (1 == this.userGameDAO.delete(userGameId)) {
			List<UserGame> gameList = userGameDAO.findByGameId(gameId);
			if ((null == gameList) || gameList.isEmpty()) {
				this.gameDAO.delete(gameId);
			}
			return true;
		}
		throw new Http500Exception();
	}
	
	/**
	 * @inheritDoc
	 */
	@Override
	public List<Game> findByUserId(String userId) throws Http204Exception {
		List<Game> gameList = this.userGameDAO.findByUserId(userId);
		if((null == gameList) || gameList.isEmpty()) {
			throw new Http204Exception();
		}
		return gameList;
	}
}
