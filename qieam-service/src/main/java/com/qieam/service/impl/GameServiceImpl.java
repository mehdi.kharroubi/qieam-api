package com.qieam.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qieam.dao.GameDAO;
import com.qieam.dao.UserGameDAO;
import com.qieam.model.Game;
import com.qieam.model.UserGame;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http500Exception;
import com.qieam.service.GameService;

/**
 * Game modification Service implementation
 * 
 * @author Mehdi KHARROUBI
 *
 */
@Service
public class GameServiceImpl implements GameService {
	
	/**
	 * User games DAO
	 */
	@Autowired
	private UserGameDAO userGameDAO;

	/**
	 * Game DAO
	 */
	@Autowired
	private GameDAO gameDAO;

	/**
	 * @inheritDoc
	 */
	@Override
	public Game post(Game game)
			throws Http500Exception {
		if (1 == this.gameDAO.insert(game)) {
			return game;
		}
		throw new Http500Exception();
	}
	
	/**
	 * @inheritDoc
	 */
	@Override
	public Game findGameById(String gameId) throws Http204Exception {
		Game game = this.gameDAO.findById(gameId);
		if(null == game) {
			throw new Http204Exception();
		}
		return game;
	}
	
	/**
	 * @inheritDoc
	 */
	@Override
	public List<Game> findAll() throws Http204Exception {
		List<Game> gameList = this.gameDAO.findAll();
		if((null == gameList) || gameList.isEmpty()) {
			throw new Http204Exception();
		}
		return gameList;
	}
	
	/**
	 * @inheritDoc
	 */
	@Override
	public boolean delete(String gameId) throws Http500Exception {
		List<UserGame> gameList = this.userGameDAO.findByGameId(gameId);
		if((null == gameList) || gameList.isEmpty()) {
			if(1 == this.gameDAO.delete(gameId)) {
				return true;
			}
		}
		else {
			if(1 == this.gameDAO.disable(gameId)) {
				return true;
			}
		}
		throw new Http500Exception();
	}
}
