package com.qieam.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qieam.dao.UserDAO;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http500Exception;
import com.qieam.service.UserService;

/**
 * User modification Service implementation
 * 
 * @author Mehdi KHARROUBI
 *
 */
@Service
public class UserServiceImpl implements UserService {

	/**
	 * User DAO
	 */
	@Autowired
	private UserDAO userDAO;

	/**
	 * @inheritDoc
	 */
	@Override
	public User post(User user) throws Http500Exception {
		if(1 == this.userDAO.insert(user)) {
			return user;
		}
		throw new Http500Exception();
	}
	
	/**
	 * @inheritDoc
	 */
	@Override
	public List<User> findAll() throws Http204Exception {
		List<User> userList = this.userDAO.findAll();
		if((null == userList) || userList.isEmpty()) {
			throw new Http204Exception();
		}
		return userList;
	}
	
	/**
	 * @inheritDoc
	 */
	@Override
	public User findByUserId(String userId) throws Http204Exception {
		User user = this.userDAO.findById(userId);
		if(null == user) {
			throw new Http204Exception();
		}
		return user;
	}
	
	/**
	 * @inheritDoc
	 */
	@Override
	public boolean delete(String userId) throws Http500Exception {
		if(1 == this.userDAO.delete(userId)) {
			return true;
		}
		throw new Http500Exception();
	}
}
