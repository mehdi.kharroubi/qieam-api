package com.qieam.service;

import java.util.List;

import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http500Exception;

/**
 * User modification Service
 * 
 * @author Mehdi KHARROUBI
 *
 */
public interface UserService {

	/**
	 * Create user
	 * 
	 * @param user : new user data
	 * @return created user
	 * @throws Exception
	 */
	User post(User user) throws Http500Exception;

	/**
	 * Find all users
	 * 
	 * @return users list
	 */
	List<User> findAll() throws Http204Exception;

	/**
	 * Find user by id
	 * 
	 * @param userId: target user
	 * @return user data
	 * @throws Exception
	 */
	User findByUserId(String userId) throws Http204Exception;

	/**
	 * Delete user operation
	 * 
	 * @param userId : target user
	 * @return operation result
	 * @throws Exception
	 */
	boolean delete(String userId) throws Http500Exception;

}