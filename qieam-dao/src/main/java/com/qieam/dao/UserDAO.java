package com.qieam.dao;

import java.util.List;

import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.qieam.model.User;

public interface UserDAO {

	@Insert("INSERT INTO public.\"user\"(\"userId\", \"firstName\", \"lastName\", \"creationDate\") "
			+ "VALUES(#{userId}, #{firstName}, #{lastName}, #{creationDate})")
	long insert(User user);
	
	@Delete("DELETE from public.\"user\" WHERE \"userId\" = #{userId}")
	long delete(@Param("userId") String userId);

    @ConstructorArgs({
	    @Arg(column = "userId", javaType = String.class),
	    @Arg(column = "firstName", javaType = String.class),
	    @Arg(column = "lastName", javaType = String.class),
	    @Arg(column = "creationDate", javaType = Long.class)
	})
    @Select("SELECT * FROM public.\"user\"")
	List<User> findAll();
    
    @ConstructorArgs({
	    @Arg(column = "userId", javaType = String.class),
	    @Arg(column = "firstName", javaType = String.class),
	    @Arg(column = "lastName", javaType = String.class),
	    @Arg(column = "creationDate", javaType = Long.class)
	})
    @Select("SELECT * FROM public.\"user\" WHERE \"userId\" = #{userId}")
    User findById(@Param("userId") String userId);
}
