package com.qieam.dao;

import java.util.List;

import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.qieam.model.Game;


public interface GameDAO {

	@Insert("INSERT INTO public.\"game\"(\"gameId\", \"title\", \"coverUrl\", \"disabled\", \"creationDate\") "
			+ "VALUES(#{gameId}, #{title}, #{coverUrl}, #{disabled}, #{creationDate})")
	long insert(Game game);
	
	@Delete("DELETE from public.\"game\" WHERE \"gameId\" = #{gameId}")
	long delete(@Param("gameId") String gameId);
	
	@Update("UPDATE public.\"game\" SET \"disabled\" = true WHERE gameId= #{gameId}")
	long disable(@Param("gameId") String gameId);

    @ConstructorArgs({
	    @Arg(column = "gameId", javaType = String.class),
	    @Arg(column = "title", javaType = String.class),
	    @Arg(column = "coverUrl", javaType = String.class),
	    @Arg(column = "disabled", javaType = Boolean.class),
	    @Arg(column = "creationDate", javaType = Long.class)
	})
    @Select("SELECT * FROM public.\"game\" WHERE \"disabled\" = false")
	List<Game> findAll();
    
    @ConstructorArgs({
	    @Arg(column = "gameId", javaType = String.class),
	    @Arg(column = "title", javaType = String.class),
	    @Arg(column = "coverUrl", javaType = String.class),
	    @Arg(column = "disabled", javaType = Boolean.class),
	    @Arg(column = "creationDate", javaType = Long.class)
	})
    @Select("SELECT * FROM public.\"game\" WHERE \"gameId\" = #{gameId} AND \"disabled\" = false")
	Game findById(@Param("gameId") String gameId);
}
