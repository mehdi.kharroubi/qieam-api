package com.qieam.dao;

import java.util.List;

import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.qieam.model.Friend;
import com.qieam.model.User;

public interface FriendDAO {

	@Insert("INSERT INTO public.\"friend\"(\"friendId\", \"firstUserId\", \"secondUserId\", \"creationDate\") "
			+ "VALUES(#{friendId}, #{firstUserId}, #{secondUserId}, #{creationDate})")
	long insert(Friend friend);
	
	@Delete("DELETE FROM public.\"friend\" WHERE \"friendId\" = #{friendId}")
	long delete(@Param("friendId") String friendId);
	
    @ConstructorArgs({
	    @Arg(column = "userId", javaType = String.class),
	    @Arg(column = "firstName", javaType = String.class),
	    @Arg(column = "lastName", javaType = String.class),
	    @Arg(column = "creationDate", javaType = Long.class)
	})
    @Select("SELECT u.* FROM public.\"friend\" f " + 
    		"JOIN public.\"user\" u ON u.\"userId\" = f.\"secondUserId\" " + 
    		"WHERE f.\"firstUserId\" = #{userId}" + 
    		"UNION  " + 
    		"SELECT u.* FROM public.\"friend\" f " + 
    		"JOIN public.\"user\" u ON u.\"userId\" = f.\"firstUserId\" " + 
    		"WHERE f.\"secondUserId\" = #{userId}")
	List<User> findByUserId(@Param("userId") String userId);
    
    
    @ConstructorArgs({
	    @Arg(column = "friendId", javaType = String.class),
	    @Arg(column = "firstUserId", javaType = String.class),
	    @Arg(column = "secondUserId", javaType = String.class),
	    @Arg(column = "creationDate", javaType = Long.class)
	})
	@Select("SELECT * FROM public.\"friend\" WHERE (\"firstUserId\" = #{firstUserId} AND \"secondUserId\" = #{secondUserId}) "
			+ "OR (\"firstUserId\" = #{secondUserId} AND \"secondUserId\" = #{firstUserId})")
    Friend findById(@Param("firstUserId") String firstUserId, @Param("secondUserId") String secondUserId);
}
