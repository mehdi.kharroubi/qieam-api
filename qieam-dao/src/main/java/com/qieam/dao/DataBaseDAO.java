package com.qieam.dao;

import org.apache.ibatis.annotations.Insert;

public interface DataBaseDAO {
	
	@Insert("CREATE TABLE IF NOT EXISTS public.game" + 
			"(" + 
			"    \"gameId\" character varying COLLATE pg_catalog.\"default\" NOT NULL," + 
			"    title character varying COLLATE pg_catalog.\"default\" NOT NULL," + 
			"    \"coverUrl\" character varying COLLATE pg_catalog.\"default\" NOT NULL," + 
			"    \"creationDate\" bigint NOT NULL," + 
			"    disabled boolean NOT NULL DEFAULT false," + 
			"    CONSTRAINT game_pkey PRIMARY KEY (\"gameId\")" + 
			")")
	void createGameTable();
	
	@Insert("CREATE TABLE IF NOT EXISTS public.\"user\"" + 
			"(" + 
			"    \"userId\" character varying COLLATE pg_catalog.\"default\" NOT NULL," + 
			"    \"firstName\" character varying COLLATE pg_catalog.\"default\" NOT NULL," + 
			"    \"lastName\" character varying COLLATE pg_catalog.\"default\" NOT NULL," + 
			"    \"creationDate\" bigint NOT NULL," + 
			"    CONSTRAINT user_pkey PRIMARY KEY (\"userId\")" + 
			")")
	void createUserTable();
	
	@Insert("CREATE TABLE IF NOT EXISTS public.\"userGame\"" + 
			"(" + 
			"    \"userGameId\" character varying COLLATE pg_catalog.\"default\" NOT NULL," + 
			"    \"gameId\" character varying COLLATE pg_catalog.\"default\" NOT NULL," + 
			"    \"userId\" character varying COLLATE pg_catalog.\"default\" NOT NULL," + 
			"    \"creationDate\" bigint NOT NULL," + 
			"    CONSTRAINT \"userGame_pkey\" PRIMARY KEY (\"userGameId\")," + 
			"    CONSTRAINT game_fk FOREIGN KEY (\"gameId\")" + 
			"        REFERENCES public.game (\"gameId\") MATCH SIMPLE" + 
			"        ON UPDATE RESTRICT" + 
			"        ON DELETE RESTRICT" + 
			"        NOT VALID," + 
			"    CONSTRAINT user_fk FOREIGN KEY (\"userId\")" + 
			"        REFERENCES public.\"user\" (\"userId\") MATCH SIMPLE" + 
			"        ON UPDATE RESTRICT" + 
			"        ON DELETE RESTRICT" + 
			"        NOT VALID" + 
			")")
	void createUserGameTable();
	
	@Insert("CREATE TABLE IF NOT EXISTS public.friend" + 
			"(" + 
			"    \"friendId\" character varying COLLATE pg_catalog.\"default\" NOT NULL," + 
			"    \"firstUserId\" character varying COLLATE pg_catalog.\"default\" NOT NULL," + 
			"    \"secondUserId\" character varying COLLATE pg_catalog.\"default\" NOT NULL," + 
			"    \"creationDate\" bigint NOT NULL," + 
			"    CONSTRAINT friend_pkey PRIMARY KEY (\"friendId\")," + 
			"    CONSTRAINT \"firstUser_fk\" FOREIGN KEY (\"firstUserId\")" + 
			"        REFERENCES public.\"user\" (\"userId\") MATCH SIMPLE" + 
			"        ON UPDATE RESTRICT" + 
			"        ON DELETE RESTRICT" + 
			"        NOT VALID," + 
			"    CONSTRAINT \"secondUser_fk\" FOREIGN KEY (\"secondUserId\")" + 
			"        REFERENCES public.\"user\" (\"userId\") MATCH SIMPLE" + 
			"        ON UPDATE RESTRICT" + 
			"        ON DELETE RESTRICT" + 
			"        NOT VALID" + 
			")")
	void createFriendTable();
}
