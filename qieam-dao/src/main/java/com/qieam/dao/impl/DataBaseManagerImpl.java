package com.qieam.dao.impl;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.qieam.dao.DataBaseDAO;
import com.qieam.dao.DataBaseManager;

@Service
public class DataBaseManagerImpl implements DataBaseManager {
	
	Logger logger = LoggerFactory.getLogger(DataBaseManagerImpl.class);
	
	@Autowired
	private DataBaseDAO dataBaseDAO;

	@PostConstruct
	private void init() {
		logger.info("Creating game table");
		this.dataBaseDAO.createGameTable();
		logger.info("Creating user table");
		this.dataBaseDAO.createUserTable();
		logger.info("Creating friend table");
		this.dataBaseDAO.createFriendTable();
		logger.info("Creating user game table");
		this.dataBaseDAO.createUserGameTable();
	}
}
