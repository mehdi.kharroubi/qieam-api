package com.qieam.dao;

import java.util.List;

import org.apache.ibatis.annotations.Arg;
import org.apache.ibatis.annotations.ConstructorArgs;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.qieam.model.Game;
import com.qieam.model.UserGame;

public interface UserGameDAO {

	@Insert("INSERT INTO public.\"userGame\"(\"userGameId\", \"gameId\", \"userId\", \"creationDate\") "
			+ "VALUES(#{userGameId}, #{gameId}, #{userId}, #{creationDate})")
	long insert(UserGame userGame);
	
	@Delete("DELETE FROM public.\"userGame\" WHERE \"userGameId\" = #{userGameId}")
	long delete(@Param("userGameId") String userId);
	
    @ConstructorArgs({
	    @Arg(column = "gameId", javaType = String.class),
	    @Arg(column = "title", javaType = String.class),
	    @Arg(column = "coverUrl", javaType = String.class),
	    @Arg(column = "disabled", javaType = Boolean.class),
	    @Arg(column = "creationDate", javaType = Long.class)
	})
    @Select("SELECT g.* FROM public.\"userGame\" ug  "
    		+ "JOIN public.\"game\" g ON g.\"gameId\" = ug.\"gameId\" WHERE ug.\"userId\" = #{userId}")
	List<Game> findByUserId(@Param("userId") String userId);
    
    @ConstructorArgs({
	    @Arg(column = "userGameId", javaType = String.class),
	    @Arg(column = "gameId", javaType = String.class),
	    @Arg(column = "userId", javaType = String.class),
	    @Arg(column = "creationDate", javaType = Long.class)
	})
	@Select("SELECT * FROM public.\"userGame\" WHERE \"gameId\" = #{gameId}")
    List<UserGame> findByGameId(@Param("gameId") String gameId);
    
    @ConstructorArgs({
	    @Arg(column = "userGameId", javaType = String.class),
	    @Arg(column = "gameId", javaType = String.class),
	    @Arg(column = "userId", javaType = String.class),
	    @Arg(column = "creationDate", javaType = Long.class)
	})
	@Select("SELECT * FROM public.\"userGame\" WHERE \"userId\" = #{userId} AND \"gameId\" = #{gameId}")
    UserGame findByUserIdAndGameId(@Param("userId") String userId, @Param("gameId") String gameId);
}

