FROM openjdk:8-jre-alpine3.9
MAINTAINER qieam-project
COPY qieam-launcher/target/qieam-launcher-2.2.6.RELEASE.jar /qieam.jar
CMD ["java", "-jar", "/qieam.jar"]