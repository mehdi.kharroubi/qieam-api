package com;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@MapperScan({"com.qieam.dao", "com.olumya.dao"})
public class QieamLauncherApplication {

	public static void main(String[] args) {
		SpringApplication.run(QieamLauncherApplication.class, args);
	}

}
