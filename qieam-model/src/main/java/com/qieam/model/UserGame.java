package com.qieam.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserGame{

	@NotBlank
	private String userGameId;
	
	@NotBlank
	private String userId;
	
	@NotBlank
	private String gameId;
	
	@NotNull
	private Long creationDate;
}
