package com.qieam.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Friend {
	
	@NotBlank
	private String friendId;
	
	@NotBlank
	private String firstUserId;
	
	@NotBlank
	private String secondUserId;
	
	@NotNull
	private Long creationDate;
}
