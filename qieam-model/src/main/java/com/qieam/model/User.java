package com.qieam.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class User {

	@NotBlank
	private String userId;
	
	@NotBlank
	private String firstName;
	
	@NotBlank
	private String lastName;

	@NotNull
	private Long creationDate;
}
