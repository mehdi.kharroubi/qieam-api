package com.qieam.model;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Game {

	@NotBlank
	private String gameId;
	
	@NotBlank
	private String title;
	
	@NotBlank
	private String coverUrl;
	
	@NotNull
	private Boolean disabled;
	
	@NotNull
	private Long creationDate;
}
