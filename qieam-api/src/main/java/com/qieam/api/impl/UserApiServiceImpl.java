package com.qieam.api.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.api.UserApiService;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http500Exception;
import com.qieam.service.UserService;
import com.qieam.validator.UserValidator;

/**
 * User Service Implementation
 * 
 * @author Mehdi KHARROUBI
 *
 */
@Service
public class UserApiServiceImpl implements UserApiService {

	/**
	 * User Modification service
	 */
	@Autowired
	private UserService userService;
	
	/**
	 * User validation service
	 */
	@Autowired
	private UserValidator userValidator;
	
	/**
	 * @inheritDoc
	 */
	@Override
	public List<User> findAll() throws Http204Exception  {
		return this.userService.findAll();
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public User post(JsonNode newUserNode) throws Http500Exception, Http400Exception  {
		return this.userService.post(this.userValidator.validatePost(newUserNode));
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean delete(String userId) throws Http400Exception, Http204Exception, Http500Exception  {
		this.userValidator.validateDelete(userId);
		return this.userService.delete(userId);
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public User findByUserId(String userId) throws Http400Exception, Http204Exception  {
		this.userValidator.validateFindByUserId(userId);
		return this.userService.findByUserId(userId);
	}
}
