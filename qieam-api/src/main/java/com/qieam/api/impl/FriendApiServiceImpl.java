package com.qieam.api.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.api.FriendApiService;
import com.qieam.model.Friend;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http409Exception;
import com.qieam.model.exception.Http500Exception;
import com.qieam.service.FriendService;
import com.qieam.validator.FriendValidator;

/**
 * Friend Service Implementation
 * 
 * @author Mehdi KHARROUBI
 *
 */
@Service
public class FriendApiServiceImpl implements FriendApiService {
	
	/**
	 * Friend modification service
	 */
	@Autowired
	private FriendService friendService;
	
	/**
	 * Friend validation service
	 */
	@Autowired
	private FriendValidator friendValidator;

	/**
	 * @inheritDoc
	 */
	@Override
	public List<User> findByUserId(String userId) throws Http400Exception, Http204Exception {
		this.friendValidator.validateFindByUserId(userId);
		return this.friendService.findByUserId(userId);
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public Friend post(String userId, JsonNode newFriendNode) throws Http500Exception, Http204Exception, Http400Exception, Http409Exception {
		return this.friendService.post(this.friendValidator.validatePost(userId, newFriendNode));
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean delete(String firstUserId, String secondUserId) throws Http400Exception, Http500Exception, Http204Exception {
		return this.friendService.delete(this.friendValidator.validateDelete(firstUserId, secondUserId));
	}
}
