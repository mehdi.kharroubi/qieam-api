package com.qieam.api.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.api.GameApiService;
import com.qieam.model.Game;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http500Exception;
import com.qieam.service.GameService;
import com.qieam.validator.GameValidator;

/**
 * Game Service Implementation
 * 
 * @author Mehdi KHARROUBI
 *
 */
@Service
public class GameApiServiceImpl implements GameApiService{
	
	/**
	 * Game modification service
	 */
	@Autowired
	private GameService gameService;
	
	/**
	 * Game validation service
	 */
	@Autowired
	private GameValidator gameValidator;

	/**
	 * @inheritDoc
	 */
	@Override
	public List<Game> findAll() throws Http204Exception {
		return this.gameService.findAll();
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public Game findByGameId(String gameId) throws Http400Exception, Http204Exception {
		this.gameValidator.validateFindGameById(gameId);
		return this.gameService.findGameById(gameId);
	}

	/** 
	 * @inheritDoc
	 */
	@Override
	public Game post(JsonNode newGameNode) throws Http500Exception, Http400Exception {
		return this.gameService.post(this.gameValidator.validatePost(newGameNode));
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean delete(String gameId) throws Http500Exception, Http400Exception, Http204Exception {
		this.gameValidator.validateDelete(gameId);
		return this.gameService.delete(gameId);
	}
}
