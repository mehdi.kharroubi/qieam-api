package com.qieam.api.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.api.UserGameApiService;
import com.qieam.model.Game;
import com.qieam.model.UserGame;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http409Exception;
import com.qieam.model.exception.Http500Exception;
import com.qieam.service.UserGameService;
import com.qieam.validator.UserGameValidator;

/**
 * User games Service implementation
 * 
 * @author Mehdi KHARROUBI
 *
 */
@Service
public class UserGameApiServiceImpl implements UserGameApiService {

	/**
	 * User game validation service
	 */
	@Autowired
	private UserGameService userGameService;
	
	/**
	 * User game validation service
	 */
	@Autowired
	private UserGameValidator userGameValidator;

	/**
	 * @inheritDoc
	 */
	@Override
	public List<Game> findByUserId(String userId) throws Http400Exception, Http204Exception  {
		this.userGameValidator.validateFindByUserId(userId);
		return this.userGameService.findByUserId(userId);
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public UserGame post(String userId, JsonNode newUserGameNode) throws Http500Exception, Http409Exception, Http204Exception, Http400Exception  {
		return this.userGameService.post(this.userGameValidator.validatePost(userId, newUserGameNode));
	}

	/**
	 * @inheritDoc
	 */
	@Override
	public boolean delete(String userId, String gameId) throws Http500Exception, Http400Exception, Http204Exception  {
		return this.userGameService.delete(gameId, this.userGameValidator.validateDelete(userId, gameId));
	}
}
