package com.qieam.api;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http500Exception;

/**
 * User Service
 * 
 * @author Mehdi KHARROUBI
 *
 */
public interface UserApiService {

	/**
	 * Find all users
	 * 
	 * @return list of users
	 * @throws Http204Exception 
	 */
	List<User> findAll() throws Http204Exception ;

	/**
	 * Create a new user
	 * 
	 * @param newUserNode : new user data
	 * @return Created user
	 * @throws Http400Exception 
	 * @throws Http500Exception 
	 */
	User post(JsonNode newUserNode) throws Http500Exception, Http400Exception ;

	/**
	 * Delete a user by id
	 * 
	 * @param userId : target user
	 * @return Operation result
	 * @throws Http204Exception 
	 * @throws Http400Exception 
	 * @throws Http500Exception 
	 */
	boolean delete(String userId) throws Http400Exception, Http204Exception, Http500Exception ;

	/**
	 * Find a user by id
	 * 
	 * @param userId : id of the target user
	 * @return user data
	 * @throws Http400Exception 
	 * @throws Http204Exception 
	 */
	User findByUserId(String userId) throws Http400Exception, Http204Exception ;

}
