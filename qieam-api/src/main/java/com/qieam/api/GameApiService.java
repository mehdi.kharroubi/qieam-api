package com.qieam.api;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.model.Game;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http500Exception;

/**
 * Game Service
 * 
 * @author Mehdi KHARROUBI
 *
 */
public interface GameApiService {

	/**
	 * Find all games in store
	 * 
	 * @return list of games
	 * @throws Http204Exception 
	 */
	List<Game> findAll() throws Http204Exception ;

	/**
	 * Find a game by id
	 * 
	 * @param gameId : id of the target game
	 * @return game data
	 * @throws Http400Exception 
	 * @throws Http204Exception 
	 */
	Game findByGameId(String gameId) throws Http400Exception, Http204Exception ;

	/**
	 * Create a new game
	 * 
	 * @param newGameNode : new game data
	 * @return Created game
	 * @throws Http400Exception 
	 * @throws Http500Exception 
	 */
	Game post(JsonNode newGameNode) throws Http500Exception, Http400Exception ;

	/**
	 * Delete a game by id
	 * 
	 * @param gameId : target game
	 * @return Operation result
	 * @throws Http400Exception 
	 * @throws Http500Exception 
	 * @throws Http204Exception 
	 */
	boolean delete(String gameId) throws Http500Exception, Http400Exception, Http204Exception;

}
