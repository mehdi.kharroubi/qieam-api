package com.qieam.api;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.model.Game;
import com.qieam.model.UserGame;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http409Exception;
import com.qieam.model.exception.Http500Exception;

/**
 * User games Service
 * 
 * @author Mehdi KHARROUBI
 *
 */
public interface UserGameApiService {

	/**
	 * Delete game for the given user
	 *
	 * @param userId : user target
	 * @param friendId : friend id
	 * @return Operation result
	 * @throws Http204Exception 
	 * @throws Http500Exception 
	 * @throws Http400Exception 
	 */
	boolean delete(String userId, String gameId) throws Http500Exception, Http204Exception, Http400Exception ;

	/**
	 * Add new game for the given user
	 * 
	 * @param newUserGameNode : game data
	 * @param userId : target user
	 * @return Created user game
	 * @throws Http400Exception 
	 * @throws Http204Exception 
	 * @throws Http409Exception 
	 * @throws Http500Exception 
	 */
	UserGame post(String userId, JsonNode newUserGameNode) throws Http500Exception, Http409Exception, Http204Exception, Http400Exception ;

	/**
	 * Find games by user
	 * 
	 * @param userId : user target
	 * @return List of all games for the given user
	 * @throws Http400Exception 
	 * @throws Http204Exception 
	 */
	List<Game> findByUserId(String userId) throws Http400Exception, Http204Exception ;

}
