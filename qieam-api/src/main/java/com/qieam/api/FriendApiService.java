package com.qieam.api;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.qieam.model.Friend;
import com.qieam.model.User;
import com.qieam.model.exception.Http204Exception;
import com.qieam.model.exception.Http400Exception;
import com.qieam.model.exception.Http409Exception;
import com.qieam.model.exception.Http500Exception;

/**
 * Friend Service
 * 
 * @author Mehdi KHARROUBI
 *
 */
public interface FriendApiService {

	/**
	 * Find friends by user
	 * 
	 * @param userId : user target
	 * @return List of all friend for the given user
	 * @throws Http400Exception 
	 * @throws Http204Exception 
	 * @throws Exception
	 */
	List<User> findByUserId(String userId) throws Http400Exception, Http204Exception;

	/**
	 * Add new friend for the given user
	 * 
	 * @param userId: user target
	 * @param newFriendNode : friend data
	 * @return Created friend
	 * @throws Http409Exception 
	 * @throws Http400Exception 
	 * @throws Http204Exception 
	 * @throws Http500Exception 
	 * @throws Exception
	 */
	Friend post(String userId, JsonNode newFriendNode) throws Http500Exception, Http204Exception, Http400Exception, Http409Exception;

	/**
	 * Delete friend for the given user
	 * 
	 * @param userId : user target
	 * @param friendId : friend id
	 * @return Operation result
	 * @throws Http204Exception 
	 * @throws Http500Exception 
	 * @throws Http400Exception 
	 */
	boolean delete(String firstUserId, String secondUserId) throws Http500Exception, Http204Exception, Http400Exception;

}
